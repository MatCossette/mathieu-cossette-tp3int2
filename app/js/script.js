var profil = {};
let submitted = true;
const donneesQuiz = 
`[{
    "numero":"1",
    "question":"Qui est Iron Man?",
    "choix":["Tony Stark", "Calvin Harris", "Donald Trump"],
    "bonneReponse": "Tony Stark" 
},{
    "numero":"2",
    "question":"Qui est Batman?",
    "choix":["Deckard Cain", "Julie Payette", "Bruce Wayne"],
    "bonneReponse": "Bruce Wayne" 
},{
    "numero":"3",
    "question":"Qui est Superman?",
    "choix":["Michael Jackson", "Clark Kent", "Benoît XVI"],
    "bonneReponse": "Clark Kent" 
},{
    "numero":"4",
    "question":"Qui est Capitaine America?",
    "choix":["Steve Rodgers", "Obi Wan Kenobi", "François Legault"],
    "bonneReponse": "Steve Rodgers" 
},{
    "numero":"5",
    "question":"Qui est Black Widow?",
    "choix":["Brian Adams", "Natasha Romanoff", "Buzz Lightyear"],
    "bonneReponse": "Natasha Romanoff" 
},{
    "numero":"6",
    "question":"Qui est Winter Soldier?",
    "choix":["Bucky Barnes", "Céline Dion", "Pumba"],
    "bonneReponse": "Bucky Barnes" 
},{
    "numero":"7",
    "question":"Qui est Scarlett Witch?",
    "choix":["Lady Gaga", "Shrek", "Wanda Maximoff"],
    "bonneReponse": "Wanda Maximoff" 
},{
    "numero":"8",
    "question":"Qui est Falcon?",
    "choix":["Jules César", "Sam Wilson", "Lucky Luke"],
    "bonneReponse": "Sam Wilson" 
}]`;
const quiz = {};
const reponses = [];
const questions = JSON.parse(donneesQuiz);
const sectionQuiz = $('<section></section>');
sectionQuiz.attr('id', 'sectionQuiz');
quiz.questionCourante = 0;
$('main').append(sectionQuiz);
$('#datatable').hide();

// Valider le formulaire
$(function valider() {
    $.validator.addMethod('dateValide', function(value,element) {
        let dateDuJour = new Date();
        return this.optional(element) || dateDuJour >= new Date(value)
    }, 'Entrez une date valide')

    $.validator.addMethod('regex', function(value, element, regexp) {
             if (regexp.constructor != RegExp)
                regexp = new RegExp(regexp);
             else if (regexp.global)
                regexp.lastIndex = 0;
                return this.optional(element) || regexp.test(value);
         },"erreur expression reguliere"
      );

    $("#formQuiz").validate({
        rules: {
            prenom: {
                required: true,
                regex: "^[a-zA-Z0-9]*$"
            },
            nom: {
                required: true,
                regex: "^[a-zA-Z0-9]*$"
            },
            date: {
                required: true,
                dateValide: true
            },
            statut: {
                required: true,
            }
        },
        messages: {
            prenom: {
                required: 'Entrez votre prénom',
                regex: 'Entrez un prénom valide'
            },
            nom: {
                required: 'Entrez un nom',
                regex: 'Entrez un nom valide'
            },
            date: {
                required: 'Entrez votre date de naissance',
            },
            statut: {
                required: 'Choisissez un statut',
            }
        },
        submitHandler: function(form) {
            profil.prenom = $('#prenom').val();
            profil.nom = $('#nom').val();
            profil.statut = $('#statut').val();
            calculerAge();
            genererQuiz();          
            $('#formulaire').hide();
        },
        showErrors: function (errorMap, errorList) {
            if (submitted) {
                const ul = $('<ul></ul>');            
                $.each(errorList, function () {ul.append(`<li>${this.message}</li>`)});
                $('#messagesErreur').html(ul);
                submitted = false
            }
            this.defaultShowErrors();
        },    
        invalidHandler: function(form, validator) {
            submitted = true;
        }
    });
});

// Calculer l'âge avec la date entrée
function calculerAge() {
    let fullDate = new Date();
    let annee = fullDate.getFullYear();
    let naissance = parseInt($('#date').val());
    let age = annee - naissance;
    profil.age = age;
}

// Faire apparaître la première question, les choix et le bouton
function genererQuiz() {
    afficherQuestion();
    // Afficher bouton
    const bouton = $('<button>Suivant</button>');
    bouton.addClass("btn btn-primary");
    bouton.attr('id', 'repondre');
    $('main').append(bouton)
    bouton.on('click', function(){
        quiz.questionCourante += 1;
        const reponse = $("input:checked").val();
        if (typeof reponse === 'undefined'){
            reponses.push('Aucune réponse')
        } else {
        reponses.push(reponse);
        }
        sectionQuiz.empty();

        if(quiz.questionCourante >= questions.length){
            afficherRapport();
            bouton.remove();
        } else {
            afficherQuestion();
        }
    })
}

// Faire apparaître chaque nouvelle question
function afficherQuestion() {

    // Animation
    $('main').css({
        opacity: 0, 
        left: -50
    });
    $('main').animate({
        opacity: 1,
        left: 0
    });

    const questionCourante = questions[quiz.questionCourante];
    const numero = (quiz.questionCourante + 1);
    const question = $('<p>' + numero + ') ' + questionCourante.question + '</p>');
    sectionQuiz.append(question);
    // Afficher choix
    for (let i = 0; i < questionCourante.choix.length; i++) {
        const divChoix = $('<div></div>');
        const choix = $('<input></input>');
        const label = $('<label>' + questionCourante.choix[i] + '</label>');
        choix.attr('type', 'radio');
        choix.attr('value', questionCourante.choix[i]);
        choix.attr('name', 'choix');
        choix.attr('required', true);
        divChoix.append(choix, label);
        sectionQuiz.append(divChoix);
    }
}

// Afficher le tableau et le score
function afficherRapport() {
    let score = 0;
    for (let i = 0; i < reponses.length; i++) {
        const tr = $('<tr></tr>');
        tr.append('<td>' + (i + 1) + '</td>');
        tr.append('<td>' + questions[i].question + '</td>');
        tr.append('<td>' + reponses[i] + '</td>');
        tr.append('<td>' + questions[i].bonneReponse + '</td>');
        if (reponses[i] === questions[i].bonneReponse) {
            const crochet = $('<td>' + "&#10003;" + '</td>');
            crochet.addClass('green');
            tr.append(crochet);
            score += 1;
        } else {
            const erreur = $('<td>' + "&#10007;" + '</td>');
            erreur.addClass('red');
            tr.append(erreur);
        }
        $('tbody').append(tr);
      }

    const modale = $('<div></div>');
    $('main').append(modale);
    modale.attr('id', 'dialog');
    if (score < (reponses.length * 0.6)) {
        modale.attr('title', 'Échec');
        modale.append($('<p>' + "Vous avez échoué le test, vous n'êtes pas assez geek. Meilleure chance la prochaine fois!" + '</p>'));
        dialog ();
    } else {
        modale.attr('title', 'Réussite');
        modale.append($('<p>' + "Vous avez réussi le test! Vous êtes officiellement geek! Bravo!" + '</p>'));
        dialog ();
    }
    // Afficher modale réussite/échec
    function dialog() {
        $( "#dialog" ).dialog({
            modale,
            show: {
                effect: "drop",
                duration: 500
              },
            resizable: false,
            draggable: false
        });
    } 

    $('#datatable').show().DataTable(
        { "language": 
            { 
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/French.json",
            "sSearchPlaceholder": "recherche..."
            },
        });
    
    const titreRésultats = $('<h3>' + 'Score: ' + score + '/' + reponses.length + '</h3>');
    $('#datatable').before(titreRésultats);
    $('#datatable').before($('<h4>' + profil.statut + ' : ' + profil.prenom + ' ' +profil.nom + ', '+ profil.age + ' ans' + '</h4>'));
    
    boutonAcc();
}

// Bouton pour faire apparaître accordion
function boutonAcc () {
    const boutonAcc = $('<button>' + 'Revoir les choix' + '</button>');
    boutonAcc.addClass('btn btn-secondary revoir');
    boutonAcc.one('click', creerAcc);
    $('main').append(boutonAcc);
}


// Accordion
function creerAcc () {
    const accordion = $('<div></div>');
    for (let i = 0; i < questions.length; i++) {
        const questionAcc = $('<h3>' + questions[i].numero + ') ' + questions[i].question + '</h3>');
        const choix = questions[i].choix;
        const choixAcc = $('<ul></ul>');
        for (let i = 0; i < choix.length; i++) {
            choixAcc.append('<li>' + choix[i] + '</li>');
        };
        accordion.append(questionAcc);
        accordion.append(choixAcc);
    }
    accordion.accordion({
        heightStyle: "content",
        collapsible: true
    });
    $('main').append(accordion)
}
