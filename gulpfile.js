const { src, dest, watch, series } = require('gulp')
const sass = require('gulp-sass')
const postcss = require('gulp-postcss')
const cssnano = require('cssnano')
const terser = require('gulp-terser')
const browsersync = require('browser-sync').create()
const autoprefixer = require('gulp-autoprefixer')

// Sass Task
function scssTask () {
  return src('app/scss/style.scss', { sourcemaps: true })
    .pipe(sass())
    .pipe(autoprefixer({
      cascade: false
    }))
    .pipe(postcss([cssnano()]))
    .pipe(dest('dist/', { sourcemaps: '.' }))
}

// JavaScript Task
function jsTask () {
  return src('app/js/script.js', { sourcemaps: true })
    .pipe(terser())
    .pipe(dest('dist/', { sourcemaps: '.' }))
}

// Browsersync Tasks
function browsersyncServe (cb) {
  browsersync.init({
    server: {
      baseDir: 'dist/'
    }
  })
  cb()
}

function browsersyncReload (cb) {
  browsersync.reload()
  cb()
}

function html () {
  return src('app/pages/*.html').pipe(dest('dist'))
}

// Watch Task
function watchTask () {
  watch('app/pages/*.html', series(html, browsersyncReload))
  watch(['app/scss/**/*.scss', 'app/js/**/*.js'], series(scssTask, jsTask, browsersyncReload))
}

// Default Gulp task
exports.default = series(
  scssTask,
  jsTask,
  browsersyncServe,
  html,
  watchTask
)
